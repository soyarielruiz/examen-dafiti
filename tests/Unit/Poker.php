<?php
namespace Tests\Unit;

use Tests\TestCase;

class Poker extends TestCase
{
    private const MAXIMUM_CARD      = 7;
    private const AS_CARD_MAX       = 14;
    private const AS_CARD_MIN       = 1;
    private const CONSEQUENT_CARDS  = 4;

    /**
     * @param $actualCard
     * @param $nextCard
     * @return bool
     */
    public static function areConsecutive($actualCard, $nextCard )
    {
        if ( $actualCard + 1 === $nextCard ) {
            return true;
        }

        return false;
    }

    /**
     * @param $cards
     * @return bool
     */
    public static function validateContinuousCards($cards)
    {
        $positionCardMax    = count($cards);
        $countedCards       = 0;

        if ( $positionCardMax > self::MAXIMUM_CARD ) {
            return false;
        }

        for ($positionCard = 0; $positionCard < $positionCardMax - 1; $positionCard++) {
            if ( self::areConsecutive($cards[$positionCard], $cards[$positionCard + 1]) ) {
                $countedCards++;
            }
        }

        if ( $countedCards == self::CONSEQUENT_CARDS ){
            return true;
        }

        return false;
    }

    /**
     * @param $values
     * @return array
     */
    public function getArray($values)
    {
        if ( !is_array($values) ){
            return explode(",", $values);
        }
        return $values;
    }

    /**
     * @param $cards
     * @return bool
     */
    public function sortCards($cards )
    {
        if ( in_array(self::AS_CARD_MAX, $cards) ) {
            $cards[] = self::AS_CARD_MIN;
        }
        sort($cards);

        return $cards;
    }

    /**
     * @param $values
     * @return bool
     */
    public function isStraight($values ) {
        $cards          = self::getArray( $values );
        $sortedCards    = self::sortCards($cards);

        return self::validateContinuousCards($sortedCards);
    }

    public function testAlgorithm() {
        $results1 = self::isStraight([2, 3, 4 ,5, 6]);
        $this->assertEquals($results1, true, "2, 3, 4 ,5, 6");
        $results2 = self::isStraight([14, 5, 4 ,2, 3]);
        $this->assertEquals($results2, true, "14, 5, 4 ,2, 3");
        $results3 = self::isStraight([7, 7, 12 ,11, 3, 4, 14]);
        $this->assertEquals($results3, false, "7, 7, 12 ,11, 3, 4, 14");
        $results4 = self::isStraight([7, 3, 2]);
        $this->assertEquals($results4, false, "7, 3, 2");
    }
}